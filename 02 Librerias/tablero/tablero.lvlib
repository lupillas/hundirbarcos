﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">5766954</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16776704</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)Z!!!*Q(C=\&gt;7^43N2%)&lt;B\S+#G\I$M%1&amp;)V'"'S$9A!9G*H.+;%I9C1K=%\G&amp;;9$!,&lt;A&amp;]ZZB-"9"4O$K#L'(Q[[`]`&gt;Q&gt;LV)&gt;@S6TL5`&gt;&gt;R^?ITW[L4P[TL6K0V&lt;?Y7(=PTJK,V(P9``U(ZK`&amp;W0KN.&lt;PW\`V(`CU/`R?`TMY`\EF_$&lt;PQ&gt;`.&amp;Z%V+1'V;GGM?Q_S:-]S:-]S:-]S)-]S)-]S)0=S:X=S:X=S:X=S)X=S)X=S)W]#LH)23ZS3-HCS5,*J-E%37=I3HYFHM34?")0(Z6Y%E`C34S*BSZ+0)EH]33?R--Q*:\%EXA34_*BKJ*E&amp;8)]C9@J&amp;8A#4_!*0)'(*26Y!E#Q7$"R-!E-"9X"2?!*0)'(3Q7?Q".Y!E`AI6G"*`!%HM!4?"B3OR+F'95=$^0)]4A?R_.Y(!^4S`%Y(M@D?"Q0S]HR/"Y(Y3TI4!Z"TC#HA`0"]4A?4H)]DM@R/"\(1V0&gt;);_&gt;':J2S0%9(M.D?!S0Y7%+'2\$9XA-D_&amp;B7BE?QW.Y$)`B93E:(M.D?!S)M3D,SZD-''BU-A,$QU]^,6:X+5JC6;2_?.50J@JB5T^%[I&gt;$@&gt;06.V.^E^3&lt;L^Z5^7;J.U(^R[H2;IR[%@8AU6%\@G_J'_K;OK)OK2.V1:V4:W0I&amp;X@=\8&lt;;&lt;L@;&lt;$:;L^&gt;;L6:;,J?;JEG,R5,T_6STW?TQ'LCA(&amp;Y)L__F?[Y@Y`LS9&lt;K[?8C_PKHT\&gt;0FS0`"`_@`Y.WI-RWPQ2[^!$M\C.A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="typeDef" Type="Folder">
			<Item Name="coordenadas.ctl" Type="VI" URL="../coordenadas.ctl"/>
			<Item Name="items.ctl" Type="VI" URL="../items.ctl"/>
		</Item>
		<Item Name="check coordenadasEnRango.vi" Type="VI" URL="../check coordenadasEnRango.vi"/>
		<Item Name="colores.vi" Type="VI" URL="../colores.vi"/>
		<Item Name="columnas.vi" Type="VI" URL="../columnas.vi"/>
		<Item Name="filas.vi" Type="VI" URL="../filas.vi"/>
		<Item Name="get tamañoTablero.vi" Type="VI" URL="../get tamañoTablero.vi"/>
		<Item Name="plantilla.vit" Type="VI" URL="../plantilla.vit"/>
		<Item Name="setCeldaColor coord.vi" Type="VI" URL="../setCeldaColor coord.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="typeDef" Type="Folder">
			<Item Name="refTablero.ctl" Type="VI" URL="../refTablero.ctl"/>
			<Item Name="tablero.ctl" Type="VI" URL="../tablero.ctl"/>
		</Item>
		<Item Name="getCoordenada.vi" Type="VI" URL="../getCoordenada.vi"/>
		<Item Name="inicializar.vi" Type="VI" URL="../inicializar.vi"/>
		<Item Name="pintarAgua.vi" Type="VI" URL="../pintarAgua.vi"/>
		<Item Name="pintarBarco.vi" Type="VI" URL="../pintarBarco.vi"/>
		<Item Name="pintarHundido.vi" Type="VI" URL="../pintarHundido.vi"/>
		<Item Name="pintarTocado.vi" Type="VI" URL="../pintarTocado.vi"/>
	</Item>
</Library>
